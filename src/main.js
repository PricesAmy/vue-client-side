// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'normalize.css/normalize.css'
import '@/style/reset.scss' // global css

import _ from 'lodash'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import VCharts from 'v-charts'
import ElementUI from 'element-ui'
import '@/my-theme/element-variables.scss'
// import 'element-ui/lib/theme-chalk/index.css'

import App from './App'
import router from './router'
import store from './store'

import * as filters from './filter/index'

// 关闭生产环境的调试信息
const isDebugMode = process.env.NODE_ENV !== 'production'
Vue.config.debug = isDebugMode
Vue.config.devtools = isDebugMode
Vue.config.productionTip = isDebugMode

Vue.use(ElementUI)
Vue.use(VCharts)
Vue.prototype._ = _

/**
 * 全局挂载公共组件
 * Require in a common component context
 */
const requireComponent = require.context(
  './components/common',
  false,
  /main-[\w-]+\.(vue|js)$/,
)
// For each matching file name...
requireComponent.keys().forEach(fileName => {
  // Get component config
  const componentConfig = requireComponent(fileName)
  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\//, '').replace(/\.\w+$/, '')),
  )
  // Register component globally
  Vue.component(componentName, componentConfig.default || componentConfig)
})

/**
 * 全局挂载过滤器
 * register global utility filters
 */
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
})
