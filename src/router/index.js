import Vue from 'vue'
import Cookies from 'js-cookie'
import VueRouter from 'vue-router'
import RouterConfig from './router.config.json'

Vue.use(VueRouter)

const registerRoute = config => {
  let arrayRoutes = []
  config.map(page =>
    arrayRoutes.push({
      meta: page.meta,
      path: page.path,
      name: page.name,
      component: resolve => require([`@/pages${page.filePath}`], resolve),
    }),
  )

  return { arrayRoutes, pages: config }
}

const routes = registerRoute(RouterConfig).arrayRoutes

const router = new VueRouter({
  mode: 'history',
  fallback: true,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash,
      }
    }
  },
})

let indexScrollTop = 0
router.beforeEach((route, redirect, next) => {
  if (route.hash !== '') {
    const id = route.hash.replace('#', '')
    const element = document.getElementById(id)
    if (element) element.scrollIntoView()
  }
  // 判断是否需要登录权限 token
  let token = '00'
  if (!token) {
    if (route.path == '/login') {
      if (route.path == '/') {
        next('/visual-reports')
      } else {
        if (route.path == '/') {
          next('/visual-reports')
        } else {
          next()
        }
      }
    } else {
      window.location.href = '/login'
    }
  } else {
    if (route.path == '/') {
      next('/visual-reports')
    } else {
      next()
    }
  }
})

router.afterEach(route => {
  if (route.hash !== '') {
    const id = route.hash.replace('#', '')
    const element = document.getElementById(id)
    if (element) element.scrollIntoView()
  }
  if (route.path !== '/') {
    document.body.scrollTop = 0
  } else {
    Vue.nextTick(() => {
      document.body.scrollTop = indexScrollTop
    })
  }
})

export default router // eslint-disable-next-line
