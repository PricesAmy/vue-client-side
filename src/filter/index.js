export const formatStatus = status => {
  switch (status) {
    case 0:
      return '驳回'
    case 1:
      return '已上传'
    case 2:
      return '已分配'
    case 3:
      return '一审完成'
    case 4:
      return '二审完成'
  }
}