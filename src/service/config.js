/*
 * @Author: shihong.lei
 * @Date: 2018-10-31 10:59:02
 * @Last Modified by: shihong.lei@advance.ai
 * @Last Modified time: 2019-02-22 13:48:00
 */
import Cookies from 'js-cookie'

let globals = {
  mockEnv: 'test',
  version: '1.0', // 版本号
  unifiedLogin: '',
  host: 'http://47.93.30.252/api', // 接口地址
  domain: '',
  hostServe: {
    test: 'http://test.lims.yoai.com/api', // 测试环境
    prod: 'http://lims.yoai.com/api', // 生产环境
  },
  domainServe: {
    test: '.yoai.com',
    prod: '.yoai.com',
  },
  loginCall: () => {
    window.localStorage.clear()
    window.sessionStorage.clear()
    Cookies.remove('tokenString')
    window.location.replace('/login')
  },
  cookieProp: {
    path: '/',
  },
  Reg: {},
  init: _ => {
    let nowHostValue = window.location.hostname
    let preIndex = nowHostValue.indexOf('.')
    let preHostName = nowHostValue.substring(0, preIndex)
    if (nowHostValue.indexOf('47.93.30.252') > -1) {
      globals.host = 'http://47.93.30.252:8080/'
      globals.domain = ''
    } else if (
      nowHostValue === 'localhost' ||
      nowHostValue.indexOf('10.16.') > -1 ||
      nowHostValue.indexOf('192.168.') > -1 ||
      nowHostValue.indexOf('172.16.') > -1 ||
      nowHostValue.indexOf('127.0.') > -1
    ) {
      let mockEnv = globals.mockEnv
      globals.host = globals.hostServe[mockEnv]
      globals.domain = ''
    } else if (preHostName === 'lims') {
      globals.host = globals.hostServe['prod']
      globals.domain = globals.domainServe['prod']
    } else {
      globals.host = globals.hostServe[preHostName]
      globals.domain = globals.domainServe[preHostName]
    }
  },
}

export default globals
