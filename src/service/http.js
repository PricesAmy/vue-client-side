/*
 * @Author: shihong.lei
 * @Date: 2018-10-24 11:54:24
 * @Last Modified by: shihong.lei@advance.ai
 * @Last Modified time: 2019-02-22 11:42:14
 */
import Vue from 'vue'
import axios from 'axios'
import globals from './config.js'
import Cookies from 'js-cookie'
import Promise from 'es6-promise'
import { Loading, Message } from 'element-ui'

Promise.polyfill()

// axios 配置
axios.defaults.timeout = 10000
axios.defaults.withCredentials = false
axios.defaults.async = true
axios.defaults.crossDomain = true
axios.defaults.headers.common['Authorization'] = window.localStorage.getItem(
  'token',
)

let loading

// axios.defaults.baseURL = globals.host
// http request 拦截器
axios.interceptors.request.use(
  config => {
    if (config.url.indexOf('order/code/check?orderCode') === -1) {
      loading = Loading.service({ text: '拼命加载中' })
    }
    // 'Access-Control-Allow-Origin', '*' // 跨域问题
    // ('Access-Control-Allow-Methods', 'GET,POST') // 跨域问题
    // config.headers['Content-Type'] = 'application/json; charset=UTF-8'
    // config.headers['Access-Control-Allow-Origin'] = '*' // 支持全域名访问，不安全，部署后需要固定限制为客户端网址
    // config.headers['Access-Control-Allow-Methods'] = 'POST,GET,OPTIONS,DELETE' // 支持的http 动作
    // config.headers['Access-Control-Allow-Headers'] =
    //   'x-requested-with,content-type' // 响应头 请按照自己需求添加。
    // config.headers['Access-Control-Allow-Credentials'] = true
    return config
  },
  err => {
    return Promise.reject(err)
  },
)

// http response 拦截器
axios.interceptors.response.use(
  response => {
    if (loading) {
      setTimeout(() => {
        Vue.nextTick(() => {
          loading.close()
        })
      }, 300)
    }
    if (response.data) {
      /**
       * @type{response.data.code}
       */
      console.log(response.data.status_code)
      if (Number(response.data.status_code) === 200) {
        // sessionId失效
        globals.loginCall('/')
        Cookies.set('status', false)
      } else if (Number(response.data.status_code) !== 0) {
        // 请求错误
        if (response.data.errors && response.data.errors.length > 0) {
          Message.error(response.data.errors)
        } else {
          Message.error('服务器错误，请您稍后再试')
        }
      }
    } else {
      Message.error('服务器错误，请您稍后再试')
    }
    return response
  },
  error => {
    if (loading) {
      Vue.nextTick(() => {
        loading.close()
      })
    }
    Message.error('服务器错误，请您稍后再试')
    return Promise.reject(error)
  },
)

export default axios
