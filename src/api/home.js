import { getData } from '../service/get-data.js'
let url = {
  getReportList: '/statistic/report', // 首页【报表】
  getIntegralReport: '/integral/report', // 积分【列表】
  getIntegralDetail: '/integral/show', // 积分【详情】
  getIntegralEdit: '/integral/edit', // 积分【详情】
  getComplainList: '/complain/index', // 投诉【列表】
}

export const getReportList = params => getData(url.getReportList, params)
export const getIntegralReport = params =>
  getData(url.getIntegralReport, params)
export const getIntegralDetail = params =>
  getData(url.getIntegralDetail, params)
export const getIntegralEdit = params => getData(url.getIntegralEdit, params)
export const getComplainList = params => getData(url.getComplainList, params)
