import { getData } from '../service/get-data.js'
let url = {
  getHabitList: '/habit/index', // 习惯【列表】
  getHabitDetail: '/habit/show', // 习惯【详情】
  getHabitAdd: '/habit/create', // 习惯【新增】
  getHabitEdit: '/habit/edit', // 习惯【编辑】
  getHabitDel: '/habit/delete', // 习惯【删除】
}

export const getHabitList = params => getData(url.getHabitList, params)
export const getHabitDetail = params => getData(url.getHabitDetail, params)
export const getHabitAdd = params => getData(url.getHabitAdd, params, 'post')
export const getHabitEdit = params => getData(url.getHabitEdit, params, 'post')
export const getHabitDel = params => getData(url.getHabitDel, params)
