import { getData } from '../service/get-data.js'
let url = {
  getOrderList: '/order/index', // 订单【列表】
  getIntegralReport: '/integral/report', // 积分【列表】
}

export const getOrderList = params => getData(url.getOrderList, params)
export const getIntegralReport = params =>
  getData(url.getIntegralReport, params)
