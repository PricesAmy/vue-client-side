import { getData } from '../service/get-data.js'
let url = {
  getRuleList: '/rule/index', // 积分规则【列表】
  getRuleStore: '/rule/store', // 积分规则【修改】
  getRuleDel: '/rule/delete', // 朋友圈【删除】
}

export const getRuleList = params => getData(url.getRuleList, params)
export const getRuleStore = params => getData(url.getRuleStore, params, 'patch')
export const getRuleDel = params => getData(url.getRuleDel, params, 'del')
