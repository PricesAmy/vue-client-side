import { getData } from '../service/get-data.js'
let url = {
  getArticleList: '/article/index', // 精选【列表】
  getArticleDetail: '/Article/show', // 精选【详情】
  getArticleAdd: '/Article/create', // 精选【新增】
  getArticleStore: '/Article/store', // 精选 【保存/编辑】
  getArticleDel: '/Article/delete', // 精选【删除】
}

export const getArticleList = params => getData(url.getArticleList, params)
export const getArticleDetail = params => getData(url.getArticleDetail, params)
export const getArticleAdd = params =>
  getData(url.getArticleAdd, params, 'post')
export const getArticleStore = params =>
  getData(url.getArticleStore, params, 'post')
export const getArticleDel = params => getData(url.getArticleDel, params, 'del')
