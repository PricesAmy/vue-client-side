import { getData } from '../service/get-data.js'
let url = {
  getActivityList: '/activity/index', // 活动【列表】
  getActivityDetail: '/activity/show', // 活动【详情】
  getActivityDel: '/activity/delete', // 活动【删除】
  getActivityRankinglist: '/activity/rankinglist', // 活动【排行榜】
}

export const getActivityList = params => getData(url.getActivityList, params)
export const getActivityDetail = params =>
  getData(url.getActivityDetail, params)
export const getActivityDel = params =>
  getData(url.getActivityDel, params, 'del')
export const getActivityRankinglist = params =>
  getData(url.getActivityRankinglist, params, 'get')
