import { getData } from '../service/get-data.js'
let url = {
  getAdminLogin: '/login', // 登录-非鉴权
  adminLogout: '/logout', // 登出-鉴权
}

export const getAdminLogin = params =>
  getData(url.getAdminLogin, params, 'post')

export const adminLogout = params => getData(url.adminLogout, params, 'del')
