import { getData } from '../service/get-data.js'
let url = {
  getPushList: '/push/index', // 推送【已推送列表】
  getPushStore: '/push/store', // 推送【修改保存】
  getPushDel: '/Push/delete', // 推送【删除】
}

export const getPushList = params => getData(url.getPushList, params)
export const getPushStore = params => getData(url.getPushStore, params, 'patch')
export const getPushDel = params => getData(url.getPushDel, params, 'del')
