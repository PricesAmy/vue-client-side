import { getData } from '../service/get-data.js'
let url = {
  getGoodsList: '/goods/index', // 商品【列表】
  getGoodsDetail: '/goods/show', // 商品【详情】
  getGoodsDel: '/goods/ delete ', // 商品【删除】
}

export const getGoodsList = params => getData(url.getGoodsList, params)
export const getGoodsDetail = params => getData(url.getGoodsDetail, params)
export const getGoodsDel = params => getData(url.getGoodsDel, params, 'post')
