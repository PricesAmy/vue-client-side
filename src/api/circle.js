import { getData } from '../service/get-data.js'
let url = {
  getFriendList: '/friend/index', // 朋友圈【列表】
  getFriendDetail: '/friend/show', // 朋友圈【详情】
  getFriendDel: '/friend/delete', // 朋友圈【删除】
}

export const getFriendList = params => getData(url.getFriendList, params)
export const getFriendDetail = params => getData(url.getFriendDetail, params)
export const getFriendDel = params => getData(url.getFriendDel, params, 'post')
