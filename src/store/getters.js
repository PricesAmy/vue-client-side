const getters = {
  orders: state => state.order.orders,
  user: state => state.user,
}
export default getters
