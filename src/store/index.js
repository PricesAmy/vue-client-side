import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import getters from './getters'
import createLogger from 'vuex/dist/logger'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

/**
 * debug是线下false，线上true
 * strict是是否开启严格模式，开启时会检测对state的修改是不是通过mutations来修改的，如果不是则会报错。
 * plugins是Vuex的插件，而createLogger是Vuex的内置的打印日志插件，在state修改后会打印各种日志，比如修改前的state是什么修改后的是什么等等
 */
const store = new Vuex.Store({
  modules: {
    app,
  },
  getters,
  strict: debug,
  plugins: debug ? [createLogger()] : [], // 开发环境下显示vuex的状态修改
})

export default store
